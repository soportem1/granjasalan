<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCompras extends CI_Model {
    public function __construct() {
        parent::__construct();
        if (isset($_SESSION['bodega_tz'])) {
            $this->bodega=$_SESSION['bodega_tz'];
        }else{
            $this->bodega=0;
        }
        //$this->DB2 = $this->load->database('other_db', TRUE); 
    }
       
    function getlistcompras($params){
        $fechainicio    = $params['fechainicio'];
        $fechafin   = $params['fechafin'];
        $columns = array( 
            0=>'comp.id_compra',
            1=>'pro.razon_social',
            2=>'comp.monto_total',
            3=>'comp.reg',
            4=>'comp.cancelado',           
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('compras comp');
        $this->db->join('proveedores pro', 'pro.id_proveedor=comp.id_proveedor');
        if($fechainicio!=''){
            $this->db->where(array('comp.reg >='=>$fechainicio.' 00:00:00'));
        }
        if($fechafin!=''){
            $this->db->where(array('comp.reg <='=>$fechafin.' 23:59:59'));   
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistcomprast($params){
        $fechainicio    = $params['fechainicio'];
        $fechafin   = $params['fechafin'];
        $columns = array( 
            0=>'comp.id_compra',
            1=>'pro.razon_social',
            2=>'comp.monto_total',
            3=>'comp.reg',
            4=>'comp.cancelado',           
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('compras comp');
        $this->db->join('proveedores pro', 'pro.id_proveedor=comp.id_proveedor');
        if($fechainicio!=''){
            $this->db->where(array('comp.reg >='=>$fechainicio.' 00:00:00'));
        }
        if($fechafin!=''){
            $this->db->where(array('comp.reg <='=>$fechafin.' 23:59:59'));   
        }

        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        
        $query=$this->db->get();
        return $query->row()->total;
    }
    function comprasdetalle($compra) {
        
        $strq = "SELECT comd.id_detalle_compra,com.reg,concat(cat.categoria,' ',mar.marca) as producto,prov.razon_social,comd.cantidad,comd.kilos,comd.precio_compra
                    FROM compra_detalle as comd
                    left join compras as com on com.id_compra=comd.id_compra
                    left join sproductosub as pros on pros.subId=comd.id_producto
                    left join sproducto as pro on pro.productoaddId=pros.productoaddId
                    left join categoria as cat on cat.categoriaId=pro.productoId
                    left join marca as mar on mar.marcaid=pro.MarcaId
                    left join proveedores as prov on prov.id_proveedor=com.id_proveedor 
                    where com.id_compra= $compra ";
        $query = $this->db->query($strq);
        return $query;
    }
    function regresarproductos($producto,$cantidad,$kilos){
        $strq = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        $subId=0;
        foreach ($query->result() as $row) {
            $subId =$row->subId;
            $PresentacionId = $row->PresentacionId;
        }
        if ($subId>0) {
            //$strq = "INSERT INTO compra_detalle(id_compra, id_producto, cantidad,kilos, precio_compra) VALUES ($idcompra,$subId,$cantidad,$kilos,$precio)";
            //$query = $this->db->query($strq);
            //$this->db->close();
            //============================================================================
            $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
            $query2 = $this->db->query($strq2);
            //$this->db->close();
            $unidad=1;
            foreach ($query2->result() as $row) {
                $unidad =$row->unidad;
            }
            //===================================
            $tounidad=$unidad*$cantidad;
            //================================
            $strq4 = "UPDATE sproductosub SET stok=stok-$tounidad WHERE productoaddId=$producto and tipo=1";
            $query4 = $this->db->query($strq4);
            //$this->db->close();
            //=====================================================
            //$strq41 = "UPDATE sproducto SET precompra=$precio WHERE productoaddId=$producto";
            //$query41 = $this->db->query($strq41);
            //$this->db->close();
            //===============================================
            //===============================================
            /*
            $strq3 = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=1";
            $query3 = $this->db->query($strq3);
            $this->db->close();
            foreach ($query3->result() as $row) {
                $stok =$row->stok;
            }

             $strq3 = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=0";
            $query3 = $this->db->query($strq3);
            $this->db->close();
            foreach ($query3->result() as $row) {
                $subId =$row->subId;
                $PresentacionId =$row->PresentacionId;

                $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
                $query2 = $this->db->query($strq2);
                $this->db->close();
                $unidad=1;
                foreach ($query2->result() as $row) {
                    $unidad =$row->unidad;
                }
                $newstok=$stok/$unidad;

                $strq4 = "UPDATE sproductosub SET stok=$newstok WHERE subId=$subId and tipo=0";
                $query4 = $this->db->query($strq4);
                $this->db->close();
            }
            */
        }
        //===================================================================
        
        /*
        $strq = "UPDATE productos SET stock=stock+$cantidad WHERE productoid=$producto";
        $query = $this->db->query($strq);
        $this->db->close();
        */
    }
    
}