<?php
ini_set("session.cookie_lifetime","86400");// 0 asta que el navegador se cierre o especificar los segundos, 86400 un dia
ini_set("session.gc_maxlifetime","86400");// 0 asta que el navegador se cierre o especificar los segundos, 86400 un dia

$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloSession extends CI_Model {
    public function __construct() {
        parent::__construct();
        if (isset($_SESSION['bodega_tz'])) {
           $this->bodega=$_SESSION['bodega_tz'];
        }else{
            $this->bodega=0;
        }
        
    }
    function login($usu,$pass) {
        $strq = "CALL SP_GET_SESSION('$usu');";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $id = $row->UsuarioID;
            $nom =$row->nombre;
            $perfil = $row->perfilId; 
            $idpersonal = $row->personalId; 
            $bodega = $row->bodega;  
            
            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                /*
                $_SESSION['usuarioid_tz']=$id;
                $_SESSION['usuario_tz']=$nom;
                $_SESSION['perfilid_tz']=$perfil;
                $_SESSION['idpersonal_tz']=$idpersonal;
                $_SESSION['bodega_tz']=$bodega;
                */
                //=====================================
                $data = array(
                        'logeado' => true,
                        'usuarioid_tz'=>$id,
                        'usuario_tz'=>$nom,
                        'perfilid_tz'=>$perfil,
                        'idpersonal_tz'=>$idpersonal,
                        'bodega_tz'=>$bodega
                    );
                $this->session->set_userdata($data);
                $count=1;
                //$count=$passwo.'/'.$id.'/'.$nom.'/'.$perfil.'/'.$idpersonal;
            }
            /*
                 si es cero aqui se agregara los mismos parametros pero para el aseso de los residentes si es que lo piden
            */
        } 
        
        echo $count;
    }
    public function menus($perfil){
        //$strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, Perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil' ORDER BY men.MenuId ASC";
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon 
                from menu as men, menu_sub as mens, personal_menu as perfd 
                where men.MenuId=mens.MenuId and perfd.MenuId=mens.MenusubId and perfd.personalId='$perfil' ORDER BY men.MenuId ASC";
        //$strq="CALL SP_GET_MENUS($perfil)";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;

    }
    public function submenus($perfil,$menu){
        $strq ="SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
                from menu_sub as menus, personal_menu as perfd 
                WHERE perfd.MenuId=menus.MenusubId and perfd.personalId='$perfil' and menus.MenuId='$menu' ORDER BY menus.MenusubId ASC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;

    }
    public function diferenteventas($user){
        $strq ="SELECT count(*) as total from personal_menu where MenuId!=4 and personalId=$user";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;

    }
    function alertproductos(){
        //$strq = "SELECT count(*) as total FROM productos where stock<=3 and activo=1";
        $strq = "SELECT count(*) as total
                FROM sproductosub as spro
                inner join sproducto as pro on pro.productoaddId=spro.productoaddId and pro.activo=1
                inner join categoria as cat on cat.categoriaId=pro.productoId
                inner join marca as mar on mar.marcaid=pro.MarcaId
                where spro.stok<10";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function alertproductosall(){
        $strq = "SELECT spro.stok,cat.categoria,mar.marca
                FROM sproductosub as spro
                inner join sproducto as pro on pro.productoaddId=spro.productoaddId and pro.activo=1
                inner join categoria as cat on cat.categoriaId=pro.productoId
                inner join marca as mar on mar.marcaid=pro.MarcaId
                where spro.stok<10";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function notas(){
        $strq = "SELECT * FROM notas where bodega=$this->bodega";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function alertcreditovencido(){
        $strq = "CALL SP_GET_TOTAL_CREDITOSVENCIDOS";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function alertcreditovencidoall(){
        $strq = "CALL SP_GET_TOTAL_CREDITOSVENCIDOS_DLL";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

}
