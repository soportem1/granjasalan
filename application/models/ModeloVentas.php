<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloVentas extends CI_Model {
    public function __construct() {
        parent::__construct();
        if (isset($_SESSION['bodega_tz'])) {
            $this->bodega=$_SESSION['bodega_tz'];
        }else{
            $this->bodega=0;
        }
        //$this->DB2 = $this->load->database('other_db', TRUE); 
    }
    function ingresarventa($uss,$cli,$total,$fac,$des,$metodo,$alias,$idventa_alias){
        $strq = "INSERT INTO ventas(id_personal, id_cliente,bodega, metodo, monto_total,facturada,descuento,alias,idventa_alias) VALUES ($uss,$cli,$this->bodega,$metodo,$total,$fac,$des,$alias,$idventa_alias)";
        $query = $this->db->query($strq);
        //$this->DB2->query($strq);
        $id=$this->db->insert_id();
        $this->db->close();
        return $id;
    }
    function ingresarventad($idventa,$producto,$cantidad,$kilos,$precio){
        if ($this->bodega=='2') {
            $stock='stok2';
        }elseif($this->bodega=='3') {
            $stock='stok3';
        }else{
            $stock='stok';
        }
        $strq = "INSERT INTO venta_detalle(id_venta, id_producto, cantidad,kilos, precio) VALUES ($idventa,$producto,'$cantidad','$kilos',$precio)";
        $query = $this->db->query($strq);
        $this->db->close();
        //$this->DB2->query($strq);
        //===================================
        $strq1 = "SELECT * FROM sproductosub where subId=$producto";
        $query1 = $this->db->query($strq1);
        //$this->DB2->query($strq1);
        $this->db->close();
        $PresentacionId=0;
        $productoaddId=0;
        foreach ($query1->result() as $row) {
            $PresentacionId =$row->PresentacionId;
            $productoaddId =$row->productoaddId;
        }
        //====================================
        $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
        $query2 = $this->db->query($strq2);
        //$this->DB2->query($strq2);
        $this->db->close();
        $unidad=1;
        foreach ($query2->result() as $row) {
            $unidad =$row->unidad;
        }
        //===================================
        $tounidad=$unidad*$cantidad;
        //================================
        $strq4 = "UPDATE sproductosub SET $stock=$stock-$tounidad WHERE productoaddId=$productoaddId and tipo=1";
        $query4 = $this->db->query($strq4);
        //$this->DB2->query($strq4);
        $this->db->close();
        //===============================================


        $strq3 = "SELECT * FROM sproductosub where productoaddId=$productoaddId and tipo=1";
        $query3 = $this->db->query($strq3);
        //$this->DB2->query($strq3);
        $this->db->close();
        foreach ($query3->result() as $row) {
            if ($this->bodega=='2') {
                $stok =$row->stok2;
            }elseif ($this->bodega=='3') {
                $stok =$row->stok3;
            }else{
                $stok =$row->stok;
            }
        }

         $strq3 = "SELECT * FROM sproductosub where productoaddId=$productoaddId and tipo=0";
        $query3 = $this->db->query($strq3);
        //$this->DB2->query($strq3);
        $this->db->close();
        foreach ($query3->result() as $row) {
            $subId =$row->subId;
            $PresentacionId =$row->PresentacionId;

            $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
            $query2 = $this->db->query($strq2);
            //$this->DB2->query($strq2);
            $this->db->close();
            $unidad=1;
            foreach ($query2->result() as $row) {
                $unidad =$row->unidad;
            }
            $newstok=$stok/$unidad;

            $strq4 = "UPDATE sproductosub SET $stock=$newstok WHERE subId=$subId and tipo=0";
            $query4 = $this->db->query($strq4);
            //$this->DB2->query($strq4);
            $this->db->close();
        }
        //==================================
    }
    function ingresarventad2($idventa,$producto,$cantidad,$kilos,$precio){
        //$this->DB2->from("ventas");
        //$idv=$this->DB2->count_all_results();
        $strq = "INSERT INTO venta_detalle(id_venta, id_producto, cantidad,kilos, precio) VALUES ($idventa,$producto,'$cantidad','$kilos',$precio)";
        //$query = $this->DB2->query($strq);
        //$this->DB2->query($strq);
        //$this->DB2->close();
    }
    function configticket(){
        $strq = "SELECT * FROM ticket";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function clientepordefecto(){
        $strq = "SELECT * FROM  clientes LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getventas($id){
        $strq = "SELECT ven.id_venta,ven.id_personal,ven.id_cliente,cli.Nom,ven.metodo,ven.bodega,ven.monto_total,  ven.facturada,ven.datosfactura,ven.descuento,ven.cancelado,ven.hcancelacion,ven.reg,ven.idventa_alias
         FROM ventas as ven 
         left join clientes as cli on cli.ClientesId=ven.id_cliente
         where ven.id_venta=$id"
         ;
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getventasd($id){
        //$strq = "CALL SP_GET_DETALLEVENTA($id)";
        $strq = "SELECT 
                vdll.id_detalle_venta,
                cat.categoria,
                mar.marca,
                pre.presentacion,
                pre.unidad,
                vdll.cantidad,
                vdll.kilos,
                vdll.precio,
                (vdll.cantidad*vdll.precio) as total
                FROM venta_detalle as vdll
                INNER JOIN sproductosub as sprob on sprob.subId=vdll.id_producto
                INNER JOIN sproducto as pro on pro.productoaddId=sprob.productoaddId
                INNER JOIN categoria as cat on cat.categoriaId=pro.productoId
                INNER JOIN marca as mar on mar.marcaid=pro.MarcaId
                INNER JOIN presentaciones as pre ON pre.presentacionId=sprob.PresentacionId
                WHERE vdll.id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function ingresarcompra($uss,$prov,$total){
        $strq = "INSERT INTO compras(id_proveedor, monto_total) VALUES ($prov,$total)";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        $this->db->close();
        return $id;
    }
    function ingresarcomprad($idcompra,$producto,$cantidad,$kilos,$precio){
        $strq = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        $subId=0;
        foreach ($query->result() as $row) {
            $subId =$row->subId;
            $PresentacionId = $row->PresentacionId;
        }
        if ($subId>0) {
            $strq = "INSERT INTO compra_detalle(id_compra, id_producto, cantidad,kilos, precio_compra) VALUES ($idcompra,$subId,$cantidad,$kilos,$precio)";
            $query = $this->db->query($strq);
            $this->db->close();
            //============================================================================
            $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
            $query2 = $this->db->query($strq2);
            $this->db->close();
            $unidad=1;
            foreach ($query2->result() as $row) {
                $unidad =$row->unidad;
            }
            //===================================
            $tounidad=$unidad*$cantidad;
            //================================
            $strq4 = "UPDATE sproductosub SET stok=stok+$tounidad WHERE productoaddId=$producto and tipo=1";
            $query4 = $this->db->query($strq4);
            $this->db->close();
            //=====================================================
            $strq41 = "UPDATE sproducto SET precompra=$precio WHERE productoaddId=$producto";
            $query41 = $this->db->query($strq41);
            $this->db->close();
            //===============================================
            //===============================================
            $strq3 = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=1";
            $query3 = $this->db->query($strq3);
            $this->db->close();
            foreach ($query3->result() as $row) {
                $stok =$row->stok;
            }

             $strq3 = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=0";
            $query3 = $this->db->query($strq3);
            $this->db->close();
            foreach ($query3->result() as $row) {
                $subId =$row->subId;
                $PresentacionId =$row->PresentacionId;

                $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
                $query2 = $this->db->query($strq2);
                $this->db->close();
                $unidad=1;
                foreach ($query2->result() as $row) {
                    $unidad =$row->unidad;
                }
                $newstok=$stok/$unidad;

                $strq4 = "UPDATE sproductosub SET stok=$newstok WHERE subId=$subId and tipo=0";
                $query4 = $this->db->query($strq4);
                $this->db->close();
            }
        }
        //===================================================================
        
        /*
        $strq = "UPDATE productos SET stock=stock+$cantidad WHERE productoid=$producto";
        $query = $this->db->query($strq);
        $this->db->close();
        */
    }
    function turnos(){
        $strq = "SELECT * FROM turno ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        $status='cerrado';
        foreach ($query->result() as $row) {
            $status =$row->status;
        }
        return $status;
    }
    function turnoss($bodega){
        $strq = "SELECT * FROM turno where bodega=$bodega ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function turnosactivo($bodega){
        $strq = "SELECT * FROM turno where bodega=$bodega ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        $status=0;
        foreach ($query->result() as $row) {
            if ($row->status=='abierto') {
                $status=1;
            }
        }
        return $status;
    }
    function abrirturno($cantidad,$nombre,$fecha,$horaa,$bodega){
        $strq = "INSERT INTO turno(fecha, horaa, cantidad, nombre, status, user,bodega) VALUES ('$fecha','$horaa','$cantidad','$nombre','abierto','user','$bodega')";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function cerrarturno($id,$horaa){
        $fechac=date('Y-m-d');
        $strq = "UPDATE turno SET horac='$horaa',fechacierre='$fechac', status='cerrado' WHERE id=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function corte($inicio,$fin,$bodega){
        if ($bodega==0) {
            $bodegaselect='';
        }else{
            $bodegaselect=' and v.bodega='.$bodega;
        }
        $strq = "SELECT cl.ClientesId,cl.Nom, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total, p.personalId, concat(p.nombre,p.apellidos) as vendedor, v.bodega,v.idventa_alias
                FROM ventas as v 
                inner join usuarios as usu on usu.UsuarioID=v.id_personal 
                inner join personal as p on p.personalId=usu.personalId
                join clientes as cl on cl.ClientesId=v.id_cliente
                WHERE v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado=0 $bodegaselect";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function cortecompras($inicio,$fin){
        $strq="SELECT compdll.id_detalle_compra,comp.reg,concat(cat.categoria,' ',mar.marca) as producto,prov.razon_social,compdll.cantidad,compdll.precio_compra 
                FROM compra_detalle as compdll 
                inner join compras as comp on comp.id_compra=compdll.id_compra 
                inner join sproductosub as pro on pro.subId=compdll.id_producto
                inner join sproducto as pr on pr.productoaddId =pro.productoaddId
                inner join categoria as cat on cat.categoriaId=pr.productoId
                inner join marca as mar on mar.marcaid=pr.MarcaId
                inner join proveedores as prov on prov.id_proveedor=comp.id_proveedor
                where comp.reg between '$inicio 00:00:00' and '$fin 23:59:59'
                ORDER BY compdll.id_detalle_compra DESC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function cortenew($inicio,$fin,$bodega){
        if ($bodega==0) {
            $bodegaselect='';
        }else{
            $bodegaselect=' and ven.bodega='.$bodega;
        }
        $strq = "SELECT categoria,presentacion,marca, cantidad, kilos,importe, tprecioc,descuento,precompra,(importe-tprecioc) as utilidad from(
                    SELECT categoria,presentacion,marca,sum(cantidad) as cantidad,sum(kilos) as kilos,sum(total) as importe,sum(totalc) as tprecioc, sum(descuento) as descuento,precompra FROM(
                        SELECT categoria,presentacion,marca,cantidad,kilos,(cantidad2*precio) as total,(cantidad2*(precompra*unidad)) as totalc,((cantidad2*precio)*descuento) as descuento,precompra FROM (
                            SELECT cat.categoria,pre.presentacion,mar.marca,vend.cantidad,if(vend.kilos=0,vend.cantidad,vend.kilos) as cantidad2,vend.kilos, if(vend.kilos=0,1,vend.kilos) as kilos2,vend.precio,ven.descuento,pro.precompra,pre.unidad
                            FROM venta_detalle as vend
                            inner JOIN ventas as ven on ven.id_venta=vend.id_venta
                            inner JOIN sproductosub as pros on pros.subId=vend.id_producto
                            inner JOIN sproducto as pro on pro.productoaddId=pros.productoaddId
                            inner JOIN categoria as cat on cat.categoriaId=pro.productoId
                            inner JOIN presentaciones as pre on pre.presentacionId=pros.PresentacionId
                            inner join marca as mar on mar.marcaid=pro.MarcaId
                            
                            WHERE ven.cancelado=0 $bodegaselect AND ven.reg BETWEEN '$inicio 00:00:00' and '$fin 23:59:59'
                        ) as con
                    ) as conn
                    GROUP BY categoria,presentacion,marca
                ) as connn";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultarturnoname($inicio,$fin){
        $strq = "SELECT * FROM turno where fecha>='$inicio' and fechacierre<='$fin'";
        $query = $this->db->query($strq);
        $this->db->close();
        //$nombre=$cfecha.'/'.$chora;
        return $query;
    }
    function cortesum($inicio,$fin,$bodega){
        if ($bodega==0) {
            $bodegaselect='';
        }else{
            $bodegaselect=' and bodega='.$bodega;
        }
        $strq = "SELECT sum(monto_total) as total , 0 as subtotal, 0 as descuento 
                FROM ventas 
                where reg between '$inicio 00:00:00' and '$fin 23:59:59' and cancelado=0 $bodegaselect";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function filas($bode) {
        if ($bode==0) {
            $bodegaselect='';
        }else{
            $bodegaselect='and bodega='.$bode;
        }
        $strq = "SELECT COUNT(*) as total FROM ventas
                 where facturada=0 $bodegaselect
                ";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginados($por_pagina,$segmento,$bodega) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        if ($bodega==0) {
            $bodegaselect='';
        }else{
            $bodegaselect='and ven.bodega='.$bodega;
        }
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,met.metodo, ven.monto_total,ven.cancelado
                FROM ventas as ven 
                inner join usuarios as usu on usu.UsuarioID=ven.id_personal 
                inner join personal as per on per.personalId=usu.personalId 
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                left join metodopago as met on met.metodoId=ven.metodo
                where ven.facturada=0 $bodegaselect
                ORDER BY ven.id_venta DESC
                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function filastur($bodega) {
        $strq = "SELECT COUNT(*) as total FROM turno where bodega=$bodega";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function cancalarventa($id){
        $fecha = date('Y-m-d');
        $strq = "UPDATE ventas SET cancelado=1,hcancelacion='$fecha' WHERE `id_venta`=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function ventadetalles($id){
        $strq = "SELECT * FROM venta_detalle where id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function regresarpro($id,$can){
        $strq = "UPDATE sproductosub set stok=stok+$can where subId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function filasturnos() {
        $strq = "SELECT COUNT(*) as total FROM turno";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginadosturnos($por_pagina,$segmento,$bodega) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT * FROM turno where bodega=$bodega ORDER BY id DESC
                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultarturno($id){
        $strq = "SELECT * FROM turno where id=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultartotalturno($fecha,$horaa,$horac,$fechac,$bodega){
        $strq = "SELECT sum(monto_total) as total FROM ventas 
                where bodega=$bodega and
                      cancelado=0 and 
                      reg between '$fecha $horaa' and '$fechac $horac' ";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        }
        return $total;
    }
    function consultartotalturno2($fecha,$horaa,$horac,$fechac){
        $strq = "SELECT sum(p.preciocompra*vd.cantidad) as preciocompra 
                from venta_detalle as vd
                inner join productos as p on vd.id_producto=p.productoid
                inner join ventas as v on vd.id_venta=v.id_venta
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac'";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->preciocompra;
        }
        return $total;
    }
    function consultartotalturnopro($fecha,$horaa,$horac,$fechac,$bodega){
        /*$strq = "SELECT concat(cat.categoria,' ',mar.marca,' ',pre.presentacion)as producto,vd.cantidad, vd.precio
                from venta_detalle as vd
                inner join sproductosub as p on vd.id_producto=p.subId
                inner join presentaciones as pre on pre.presentacionId=p.PresentacionId
                inner join sproducto as pro on pro.productoaddId=p.productoaddId
                inner join categoria as cat on cat.categoriaId=pro.productoId
                inner join marca as mar on mar.marcaid=pro.MarcaId
                inner join ventas as v on vd.id_venta=v.id_venta
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac'";*/
        $strq="CALL SP_GET_PRODUCTOS_TURNO('$fecha $horaa','$fechac $horac',$bodega)";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultartotalturnopromas($fecha,$horaa,$horac,$fechac,$bodega){
        /*$strq = "SELECT p.nombre as producto, sum(vd.cantidad) as total 
                from venta_detalle as vd 
                inner join productos as p on vd.id_producto=p.productoid 
                inner join ventas as v on vd.id_venta=v.id_venta 
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac' GROUP BY producto ORDER BY `total` DESC LIMIT 1"; */
        $strq ="CALL SP_GET_PRODUCTOMAS_TURNO('$fecha $horaa','$fechac $horac',$bodega)";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function filaslcompras(){
        $strq = "SELECT COUNT(*) as total FROM compra_detalle";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginadoslcompras($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT comd.id_detalle_compra,com.reg,concat(cat.categoria,' ',mar.marca) as producto,prov.razon_social,comd.cantidad,comd.kilos,comd.precio_compra
                    FROM compra_detalle as comd
                    left join compras as com on com.id_compra=comd.id_compra
                    left join sproductosub as pros on pros.subId=comd.id_producto
                    left join sproducto as pro on pro.productoaddId=pros.productoaddId
                    left join categoria as cat on cat.categoriaId=pro.productoId
                    left join marca as mar on mar.marcaid=pro.MarcaId
                    left join proveedores as prov on prov.id_proveedor=com.id_proveedor 
                ORDER BY comd.id_detalle_compra DESC
                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function lcomprasconsultar($inicio,$fin) {
        
        $strq = "SELECT comd.id_detalle_compra,com.reg,concat(cat.categoria,' ',mar.marca) as producto,prov.razon_social,comd.cantidad,comd.kilos,comd.precio_compra
                    FROM compra_detalle as comd
                    left join compras as com on com.id_compra=comd.id_compra
                    left join sproductosub as pros on pros.subId=comd.id_producto
                    left join sproducto as pro on pro.productoaddId=pros.productoaddId
                    left join categoria as cat on cat.categoriaId=pro.productoId
                    left join marca as mar on mar.marcaid=pro.MarcaId
                    left join proveedores as prov on prov.id_proveedor=com.id_proveedor  
                where com.reg between '$inicio 00:00:00' and '$fin 23:59:59'
                ORDER BY comd.id_detalle_compra DESC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function ventassearch($search,$bode){
        if ($bode==0) {
            $bodegaselect='';
        }else{
            $bodegaselect='and ven.bodega='.$bode;
        }
        $strq="SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,met.metodo, ven.monto_total,ven.cancelado
                FROM ventas as ven 
                inner join usuarios as usu on usu.UsuarioID=ven.id_personal 
                inner join personal as per on per.personalId=usu.personalId 
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                left join metodopago as met on met.metodoId=ven.metodo
                where ven.facturada=0 $bodegaselect and ven.id_venta like '%".$search."%' or 
                      ven.facturada=0 $bodegaselect and ven.reg like '%".$search."%' or
                      ven.facturada=0 $bodegaselect and cli.Nom like '%".$search."%' or
                      ven.facturada=0 $bodegaselect and per.nombre like '%".$search."%' or
                      ven.facturada=0 $bodegaselect and ven.monto_total like '%".$search."%'
                ";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    //==================================facturadas
    function filasf($bode) {
         if ($bode==0) {
            $bodegaselect='';
        }else{
            $bodegaselect='and ven.bodega='.$bode;
        }
        $strq = "SELECT COUNT(*) as total FROM ventas as ven
                where ven.facturada=1 $bodegaselect
                ";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginadosf($por_pagina,$segmento,$bode) {
        if ($bode==0) {
            $bodegaselect='';
        }else{
            $bodegaselect='and ven.bodega='.$bode;
        }
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,met.metodo, ven.monto_total,ven.cancelado
                FROM ventas as ven 
                inner join usuarios as usu on usu.UsuarioID=ven.id_personal 
                inner join personal as per on per.personalId=usu.personalId 
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                left join metodopago as met on met.metodoId=ven.metodo
                where ven.facturada=1 $bodegaselect
                ORDER BY ven.id_venta DESC
                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function ventassearchf($search,$bodega){
        if ($bodega==0) {
            $bodegaselect='';
        }else{
            $bodegaselect='and ven.bodega='.$bode;
        }
        $strq="SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,met.metodo, ven.monto_total,ven.cancelado
                FROM ventas as ven 
                inner join usuarios as usu on usu.UsuarioID=ven.id_personal 
                inner join personal as per on per.personalId=usu.personalId 
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                left join metodopago as met on met.metodoId=ven.metodo
                where ven.facturada=1 $bodegaselect and ven.id_venta like '%".$search."%' or 
                      ven.facturada=1 $bodegaselect and ven.reg like '%".$search."%' or
                      ven.facturada=1 $bodegaselect and cli.Nom like '%".$search."%' or
                      ven.facturada=1 $bodegaselect and per.nombre like '%".$search."%' or
                      ven.facturada=1 $bodegaselect and ven.monto_total like '%".$search."%'
                ";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function datosfactura($idventa,$datos){
        $strq="CALL SP_ADD_DATOSFACTURA($idventa,'$datos')";
        $query = $this->db->query($strq);
        $this->db->close();
        return $strq;
    }
    function datosfacturaget($id){
        $strq="SELECT datosfactura FROM ventas where id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    // para corte

    function totalcajas($id){
         $strq = "SELECT pre.presentacion,sum(vend.cantidad) as cantidad
                    from venta_detalle as vend
                    inner join sproductosub as spro on spro.subId=vend.id_producto
                    inner join presentaciones as pre on pre.presentacionId=spro.PresentacionId
                    WHERE vend.id_venta=$id
                    GROUP BY pre.presentacion";
        $query = $this->db->query($strq);
        $this->db->close();
        $presentacionestotal='';
        foreach ($query->result() as $pres) {
           $presentacionestotal.='<p>'.$pres->presentacion.' : '.$pres->cantidad.'</p>'; 
        }
        return $presentacionestotal; 
    }
    function totalkilos($id){
         $strq = "SELECT sum(kilos) as total FROM venta_detalle where id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }


    
    function getlistventas($params){
        /*
            SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,met.metodo, ven.monto_total,ven.cancelado
                FROM ventas as ven 
                inner join usuarios as usu on usu.UsuarioID=ven.id_personal 
                inner join personal as per on per.personalId=usu.personalId 
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                left join metodopago as met on met.metodoId=ven.metodo
                where ven.facturada=0
        */

        $columns = array( 
            0=>'ven.id_venta',
            1=>'ven.reg', 
            2=>"concat(per.nombre,' ',per.apellidos) as vendedor",
            3=>'ven.monto_total',
            4=>'met.metodo',
            5=>'cli.Nom as cliente',
            6=>'ven.cancelado',  
            7=>'ven.idventa_alias'          
        );
        $columnsss = array( 
            0=>'ven.id_venta',
            1=>'ven.reg', 
            2=>"concat(per.nombre,' ',per.apellidos)",
            3=>'ven.monto_total',
            4=>'met.metodo',
            5=>'cli.Nom',
            6=>'ven.cancelado', 
            7=>'ven.idventa_alias'           
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('ventas ven');
        $this->db->join('usuarios usu', 'usu.UsuarioID=ven.id_personal');
        $this->db->join('personal per', 'per.personalId=usu.personalId');
        $this->db->join('clientes cli', 'cli.ClientesId=ven.id_cliente');
        $this->db->join('metodopago met', 'met.metodoId=ven.metodo');
      
        $this->db->where(array('ven.facturada'=>0));
        if($params['dia']==1){
            $this->db->where(array('ven.alias'=>1));
        }else if($params['dia']==2){
            $this->db->where(array('ven.alias'=>0));
        }
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistventast($params){
        $columns = array( 
            0=>'ven.id_venta',
            1=>'ven.reg', 
            2=>"concat(per.nombre,' ',per.apellidos)",
            3=>'ven.monto_total',
            4=>'met.metodo',
            5=>'cli.Nom',
            6=>'ven.cancelado',
            7=>'ven.idventa_alias'            
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('ventas ven');
        $this->db->join('usuarios usu', 'usu.UsuarioID=ven.id_personal');
        $this->db->join('personal per', 'per.personalId=usu.personalId');
        $this->db->join('clientes cli', 'cli.ClientesId=ven.id_cliente');
        $this->db->join('metodopago met', 'met.metodoId=ven.metodo');

        $this->db->where(array('ven.facturada'=>0));
        
        if($params['dia']==1){
            $this->db->where(array('ven.alias'=>1));
        }else if($params['dia']==2){
            $this->db->where(array('ven.alias'=>0));
        }
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    
}