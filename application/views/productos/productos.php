<style type="text/css">
  .imgpro{
    width: 80px;
  }
  .imgpro:hover {
    transform: scale(2.6); 
    filter: drop-shadow(5px 9px 12px #444);
  }
  #tableproductosa_wrapper{
    padding: 0px;
  }
  #tableproductosa td{
    font-size: 14px;
  }
  #tableproductosa th{
    
  }
</style>
<input type="hidden" id="base_url" value="<?php echo base_url(); ?>Productos/">
<div class="row">
                <div class="col-md-12">
                  <h2>Producto</h2>
                  <?php //echo $productosp;?>
                </div>
                
                <div class="col-md-12">
                  <div class="col-md-9"></div>
                  <div class="col-md-3">
                    <a href="<?php echo base_url(); ?>Productos/productosadd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo</a>
                    <button class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow" onclick="productosall()"><i class="fa fa-print"></i></button>
                    <button class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow" onclick="productosallprecios()"><i class="fa fa-dollar"></i></button>
                    <button class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow" onclick="productosallexcel()"><i class="fa fa-file-excel-o"></i></button>
                  </div>
                </div>
                
                
              </div>
              <!--Statistics cards Ends-->

              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Listado de productos</h4>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-8">
                            
                          </div>
                          
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <!--------//////////////-------->
                              
                                    <!------------------------------------------>
                                    <table class="table table-striped" id="tableproductosa" style="width: 100%">
                                      <thead>
                                        <tr>
                                          <th>#</th>
                                          <th></th>
                                          <th>Nombre</th>
                                          <th>Marca</th>
                                          <th>Stock</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody id="tbodyresultadospro">
                                      </tbody>
                                    </table>
                        <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>
<!------------------------------------------------>
<script type="text/javascript">
        $(document).ready(function() {
                $('#data-tabless').dataTable();
                $('#imprimiretiqueta').click(function(event) {
                  var idp=$('#idproetiqueta').val();
                  var nump=$('#numprint').val();
                  $('#iframeetiqueta').html('<iframe src="<?php echo base_url(); ?>Etiquetas?id='+idp+'&page='+nump+'&print=true"></iframe>');
                });
                $('#sieliminar').click(function(event) {
                  var idp =$('#hddIdpro').val();
                    $.ajax({
                        type:'POST',
                        url: '<?php echo base_url(); ?>Productos/deleteproductos',
                        data: {id:idp},
                        async: false,
                        statusCode:{
                            404: function(data){
                                toastr.error('Error!', 'No Se encuentra el archivo');
                            },
                            500: function(){
                                toastr.error('Error', '500');
                            }
                        },
                        success:function(data){
                            console.log(data);
                            //location.reload();
                            toastr.success('Hecho!', 'eliminado Correctamente');
                            loadtable();
                            
                        }
                    });
                });
        } );
        function productodelete(id){
          $('#hddIdpro').val(id);
          $('#eliminacion').modal();
          
      }
      function etiquetas(id,codigo,nombre,categoria,precio){
        $('#ecodigo').html(codigo);
        $('#eproducto').html(nombre);
        $('#ecategoria').html(categoria);
        $('#eprecio').html(precio);
        $('#idproetiqueta').val(id);

        $("#modaletiquetas").modal();
        $('#iframeetiqueta').html('<iframe src="<?php echo base_url(); ?>Etiquetas?id='+id+'&page=1"></iframe>'); 
      }
      function mermas(id){
        $('#promermerma').val(id);
        $('#modalmermas').modal();
      }
      function traspaso(id,bodega1,bodega2,bodega3){
        $('#protraspaso').val(id);
        var agregarstoc='<option value="0"></option><option value="1">Matriz ('+bodega1+')</option>';
            agregarstoc+='<option value="2">Sucursal 1 ('+bodega2+')</option>';
            agregarstoc+='<option value="3">Sucuarsal 2 ('+bodega3+')</option>';
            $('#bodegatraspaso').html(agregarstoc);
        $('#modaltraspaso').modal();
      }
      function productosall(){
        $("#modalproductos").modal();
        $('#iframeproductos').html('<iframe src="<?php echo base_url(); ?>Visorpdf?filex=Productosall&iden=id&id=0" style="height: 500px "></iframe>'); 
      }
      function productosallprecios(){
        $("#modalproductos").modal();
        $('#iframeproductos').html('<iframe src="<?php echo base_url(); ?>Visorpdf?filex=Productosall/precios&iden=id&id=0" style="height: 500px "></iframe>'); 
      }
      function productosallexcel(){
        window.open("<?php echo base_url(); ?>Productosall/excel","_blank");
      }
      function deshabilitar(){
        var option=$('#bodegatraspaso option:selected').val();
        $('#bodegatraspaso2 option').attr("disabled", false);
        $('#bodegatraspaso2 option[value="'+option+'"]').attr("disabled", true);
      }

</script>
<style type="text/css">
  iframe{
        width: 100%;
        height: 300px;
        border: 0;
  }
</style>
<!--
<div class="modal fade text-left" id="modaletiquetas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Etiquetas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="col-md-6">
                <p>Codigo de producto: <b><span id="ecodigo"></span></b> </p>
                <p>Producto: <b><span id="eproducto"></span></b></p>
                <p>Categoria: <b><span id="ecategoria"></span></b></p>
                <p>Precio: <b>$ <span id="eprecio"></span></b></p>
                <input type="hidden" name="idproetiqueta" id="idproetiqueta" readonly>
                <div class="col-md-12">
                  <div class="form-group">
                    <div class=" col-md-6">
                        <input type="number" name="numprint" id="numprint" class="form-control" min="1" value="1">
                    </div>
                    <div class=" col-md-5">
                      <a href="#" class="btn btn-raised gradient-purple-bliss white" id="imprimiretiqueta">Imprimir</a>
                    </div>
                  </div>
                </div>

              </div>
              <div class="col-md-6" id="iframeetiqueta">
                
              </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>-->
<div class="modal fade text-left" id="eliminacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmaci&oacute;n</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea <b>Eliminar</b> el producto ?
                      <input type="hidden" id="hddIdpro">
            </div>
            <div class="modal-footer">
                <button type="button" id="sieliminar" class="btn btn-raised gradient-purple-bliss white" data-dismiss="modal">Aceptar</button>
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modalproductos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Productos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
              <div class="col-md-12" id="iframeproductos">
                
              </div>

           <!-- </div>-->
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modalmermas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mermas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                  <input type="hidden" id="promermerma">
                  <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-lg-3">Tipo de merma</label>
                        <div class="col-lg-9">
                          <select class="form-control" id="marcamerma">
                            <option value="21">Yema</option>
                            <option value="22">Cascado</option>
                          </select>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-lg-3">Presentacion</label>
                        <div class="col-lg-9">
                          <select class="form-control" id="presmerma">
                            <option value="28">CUBETA</option>
                            <option value="1">CAJA</option>
                            <option value="2">1/2 CAJA</option>
                            <option value="3">12 CONOS</option>
                          </select>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-lg-3">Cantidad</label>
                        <div class="col-lg-9">
                          <input type="number" class="form-control" id="cantidadmerma">
                        </div>
                    </div>
                  </div>
              </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-primary" id="add_merma" data-dismiss="modal">Aceptar</button>
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modaltraspaso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Traspasos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                  <input type="hidden" id="protraspaso">
                  <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-lg-3">Sucuarsal Origen</label>
                        <div class="col-lg-9">
                          <select class="form-control" id="bodegatraspaso" onchange="deshabilitar()">
                          </select>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-lg-3">Sucursal Destino</label>
                        <div class="col-lg-9">
                          <select class="form-control" id="bodegatraspaso2">
                            <option value="0"></option>
                            <option value="1">Matriz</option>
                            <option value="2">Sucuarsal 1</option>
                            <option value="3">Sucuarsal 2</option>
                          </select>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-lg-3">Cantidad</label>
                        <div class="col-lg-9">
                          <input type="number" class="form-control" id="cantidadtraspaso">
                        </div>
                    </div>
                  </div>
              </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-primary" id="add_traspaso" data-dismiss="modal">Aceptar</button>
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>