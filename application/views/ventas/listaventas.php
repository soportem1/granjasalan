<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/print.css" media="print">
<style type="text/css">
    .personalbodega{
        display: none;
    }
    .div_bottms{
        width: 128px;
    }
    #data-tables-ventas td{
        font-size: 12px;
    }
</style>
<div class="row">
                <div class="col-md-12">
                  <h2>Lista ventas</h2>
                </div>
              </div>
              <!--Statistics cards Ends-->

              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Listado de ventas</h4>
                        </div>
                        <div class="col-md-12 inputbusquedas">
                          <div class="col-md-11">
                            
                          </div>
                          <div class="col-md-1">
                            <!--<a id="btnImprimir" onclick="imprimir();"><button type="button" class="btn btn-raised gradient-purple-bliss white"  style="background: #2e58a6;" ><i class="fa fa-print"></i></button></a>-->
                          </div>
                          <div class="col-md-2 personalbodega">
                            <select class="form-control" id="personalbodega" onchange="seleccionar()">
                              <option value="0">Todo</option>
                              <option value="1" <?php if (isset($_GET['bod'])) { if ($_GET['bod']==1) { echo "selected"; } }?>  >Matriz</option>
                              <option value="2" <?php if (isset($_GET['bod'])) { if ($_GET['bod']==2) { echo "selected"; } }?> >Sucursal 1</option>
                              <option value="3" <?php if (isset($_GET['bod'])) { if ($_GET['bod']==3) { echo "selected"; } }?> >Sucursal 2</option>
                            </select>
                          </div>
                          <div class="col-md-2">
                            <select class="form-control" id="tipo_dia" onchange="seleccionar_dia()">
                              <option value="0">Todo</option>
                              <option value="1">Domingo</option>
                              <option value="2">Lunes a sábado</option>
                            </select>
                          </div>
                          
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <!--------//////////////-------->
                                <table class="table table-striped" id="data-tables-ventas" style="width: 100%">
                                      <thead>
                                        <tr>
                                          <th>Folio</th>
                                          <th>Fecha</th>
                                          <th>Vendedor</th>
                                          <th>Monto</th>
                                          <th>Metodo</th>
                                          <th>Cliente</th>
                                          <th></th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody></tbody>
                                </table>    
                        <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>
<!------------------------------------------------>
<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>
<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <div id="iframereporte"></div>
            <!--</div>-->
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="cancelar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmaci&oacute;n</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea cancelar el ticket No. <span id="NoTicket"></span> por un total de <span id="CantidadTicket"></span>?
                      <input type="hidden" id="hddIdVenta">
            </div>
            <div class="modal-footer">
                <button type="button" id="sicancelar" class="btn btn-raised gradient-purple-bliss white" data-dismiss="modal">Aceptar</button>
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modaledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body itemsventasedit">
            </div>
            <div class="modal-footer">
                <button type="button" id="sieditar" class="btn btn-raised gradient-purple-bliss white">Aceptar</button>
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  $(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    
  });
  function ticket(id){
    $("#iframeri").modal();
    $('#iframereporte').html('<iframe src="<?php echo base_url(); ?>Visorpdf?filex=Ticket&iden=id&id='+id+'"></iframe>');
  }
  function cancelar(ID,CantidadTicket){
    $('#cancelar').modal();
    $("#hddIdVenta").val(ID);
    $("#NoTicket").html(ID);
    $("#CantidadTicket").html(CantidadTicket);  
  }

  function seleccionar(){
    var bode=$('#personalbodega option:selected').val();
    location.href="<?php echo base_url(); ?>ListaVentas?bod="+bode;
  }

  function seleccionar_dia(){
    loadtable();  
  }

  function imprimir(){
      window.print();
  }

  
</script>