<style type="text/css">
    #data-tables td,#data-tables-compras td{
        font-size: 13px;
    }
    #data-tables-compras{
      margin-right: 1px;
    }
    #data-tables-compras_wrapper{
      padding: 0px;
    }
</style>
<div class="row">
  <div class="col-md-12">
    <h2>Compras</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Listado de Compras</h4>
          </div>
          <div class="card-body">
              <div class="card-block">
                  <!--------//////////////-------->
                  <div class="col-md-3">
                    <input type="date" id="productoscin" class="form-control" onchange="loadtable()">
                  </div>
                  <div class="col-md-3">
                    <input type="date" id="productoscfin" class="form-control" onchange="loadtable()">
                  </div>
                  <div class="col-md-3">
                    <a class="btn btn-raised gradient-purple-bliss white" id="buscarlproducto" style="background: #2e58a6;">Consultar</a>
                  </div>
                  <table class="table table-striped" id="data-tables-compras">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Proveedor</th>
                        <th>Total</th>
                        <th>Fecha</th>
                        <th></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                  
          <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>
<div class="modal fade text-left" id="modalcompraedit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Compra</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modalcompraedit">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>