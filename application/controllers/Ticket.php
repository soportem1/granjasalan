<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Ticket extends CI_Controller {
	function __construct(){
        parent::__construct();
        //$this->load->model('Solicitud');
        $this->load->model('ModeloVentas');
    }
	public function index(){
		$id=$this->input->get('id');
		
		$data['configticket']=$this->ModeloVentas->configticket();
		$data['getventas']=$this->ModeloVentas->getventas($id);
		$data['getventasd']=$this->ModeloVentas->getventasd($id);
		$this->load->view('Reportes/ticket',$data);
	        
	}
}