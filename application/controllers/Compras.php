<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compras extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProveedor');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloVentas');
    }
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('compras/compras');
        $this->load->view('templates/footer');
        $this->load->view('compras/jscompras');
	}
    public function searchpro(){
        $usu = $this->input->get('search');
        $results=$this->ModeloProveedor->proveedorallsearch($usu);
        echo json_encode($results->result());
    }
    
    
    public function addproducto(){
        $cant = $this->input->post('cant');
        $prod = $this->input->post('prod');
        $prec = $this->input->post('prec');
        $prodt = $this->input->post('prodt');
        //$personalv=$this->ModeloProductos->getproducto($prod);
        //foreach ($personalv->result() as $item){
        //      $id = $item->productoid;
        //      $precio = $item->preciocompra;
        //}
        $array = array("id"=>$prod,
                        "cant"=>$cant,
                        "producto"=>$prodt,
                        "precio"=>$prec
                    );
            echo json_encode($array);
    }
    function ingresarcompra(){
        $uss = $this->input->post('uss');
        $prov = $this->input->post('prov');
        $total = $this->input->post('total');
        $id=$this->ModeloVentas->ingresarcompra($uss,$prov,$total);
        echo $id;
    }
    function ingresarcomprapro(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idcompra = $DATA[$i]->idcompra;
            $producto = $DATA[$i]->producto;
            $cantidad = $DATA[$i]->cantidad;
            $kilos = $DATA[$i]->kilos;
            $precio = $DATA[$i]->precio;
            $this->ModeloVentas->ingresarcomprad($idcompra,$producto,$cantidad,$kilos,$precio);
        }
    }

}