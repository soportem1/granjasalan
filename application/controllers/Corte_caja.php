<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Corte_caja extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->model('Personal/ModeloPersonal');
        //$this->load->model('Usuarios/ModeloUsuarios');
        $this->load->model('ModeloVentas');
    }
	public function index(){
            //$data['personal']=$this->ModeloPersonal->getpersonal();
            //carga de vistas
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);

            $this->load->view('corte/corte');
            $this->load->view('templates/footer');
            $this->load->view('corte/jscorte');
	}
    function corte(){
        $inicio = $this->input->post('fecha1');
        $fin = $this->input->post('fecha2');
        $bodega = $this->input->post('bodega');
        $informe = $this->input->post('informe');
        $resultadoc=$this->ModeloVentas->corte($inicio,$fin,$bodega);
        $resultadocompras=$this->ModeloVentas->cortecompras($inicio,$fin);
        $resultadocs=$this->ModeloVentas->cortesum($inicio,$fin,$bodega);
        $nuevocorte=$this->ModeloVentas->cortenew($inicio,$fin,$bodega);
            $table="";
            $rowventas=0;
        if ($informe==1) {
            $table.="<table class='table table-striped table-bordered table-hover' id='sample_2'>
                        <thead>
                            <tr>
                                <th>No. venta</th>
                                <th>Cajero</th>
                                <th>Cliente</th>
                                <th>Fecha</th>
                                <th>Sucursal</th>
                                <th></th>
                                <th></th>
                                <th>Total</th>
                                <th></th>
                                
                            </tr>
                        </thead>
                        <tbody>";
            
            $monto_totalf=0;
            foreach ($resultadoc->result() as $fila) { 
                switch ($fila->bodega) {
                    case 1:
                        $bodegas='Matriz';
                        break;
                    case 2:
                        $bodegas='Sucursal 1';
                        break;
                    case 3:
                        $bodegas='Sucursal 2';
                        break;
                    
                    default:
                        $bodegas='';
                        break;
                }
                /*
                $kilos=$this->ModeloVentas->totalkilos($fila->id_venta);
                if ($kilos>0) {
                    $kilosl='Kilos: '.number_format($kilos,2,'.',',');
                }else{
                    $kilosl='';
                }
                */
                $totalprecentacion=$this->ModeloVentas->totalcajas($fila->id_venta);
                $productosd='';
                $getventasd=$this->ModeloVentas->getventasd($fila->id_venta);
                foreach ($getventasd->result() as $rowEmp){
                    if ($rowEmp->kilos>0) {
                        $rowkilos = $rowEmp->kilos.' kg';
                    }else{
                        $rowkilos='';
                    }
                    $productosd.='<p>'.$rowEmp->cantidad.' / '.$rowEmp->categoria.' '.$rowEmp->marca.' '.$rowEmp->presentacion.' '.$rowkilos.'</p>';
                }
                

                $table .= "<tr>
                                <td>".$fila->idventa_alias."</td>
                                <td>".$fila->vendedor."</td>
                                <td>".$fila->Nom."</td>
                                <td>".$fila->reg."</td>
                                <td>".$bodegas."</td>
                                <td>".$productosd."</td>
                                <td>".$totalprecentacion."</td>
                                <td>".number_format($fila->monto_total,2,'.',',')."</td>
                                <td>
                                    <button class='btn btn-raised gradient-blackberry white sidebar-shadow' onclick='ticket(".$fila->id_venta.")'' title='' data-toggle='tooltip' data-placement='top' data-original-title='Ticket'>
                                                          <i class='fa fa-book'></i>
                                                        </button>
                                </td>
                                
                            </tr>";
                            $rowventas++;
                            $monto_totalf=$monto_totalf+$fila->monto_total;
            }
            $table.="</tbody> 
                     <tfoot>
                        <tr>
                            <td>Ventas:</td>
                            <td>".$rowventas."</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Total:</td>
                            <td><b>$ ".number_format($monto_totalf,2,'.',',')."</b></td>
                            <td></td>
                        </tr>
                    </tfoot>

            </table> <br><br><br>";
        }
        if ($informe==0) {
            $table .="<table class='table table-striped table-bordered table-hover' id='sample_4'>
                    <thead>
                        <tr>
                            <th></th>
                            <th>PIEZAS</th>
                            <th>KILOS</th>
                            <th>IMPORTE</th>
                            <th>COMPRA</th>
                            <th>ESTIMADA</th>
                            <th>GANANCIA</th>
                        </tr>
                    </thead>
                    <tbody>";
                    $totalproductos=0;
                foreach ($nuevocorte->result() as $fila) { 
                    if ($fila->kilos>0) {
                        $kilosl2='Kilos: '.number_format($fila->kilos,2,'.',',');
                    }else{
                        $kilosl2='';
                    }
                    $total3 =$fila->importe-$fila->descuento;
                    
                    $table .="<tr>
                            <td>".$fila->categoria." ".$fila->marca." ".$fila->presentacion."</td>
                            <td>".$fila->cantidad."</td>
                            <td>".$kilosl2."</td>
                            <td>".number_format($total3,2,'.',',')."</td>
                            <td>".number_format($fila->precompra,2,'.',',')."</td>
                            <td>".number_format($fila->tprecioc,2,'.',',')."</td>
                            <td>".number_format($fila->utilidad,2,'.',',')."</td>
                        </tr>";
                        $totalproductos=$totalproductos+$total3;
                }
                 $table .="</tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>".number_format($totalproductos,2,'.',',')."</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                </table>";
        }
        if ($informe==2) {
            $table.="<table class='table table-striped table-bordered table-hover' id='sample_3'>
                        <thead>
                            <tr>
                                <th>No. Compra</th>
                                <th>Fecha</th>
                                <th>Producto</th>
                                <th>Proveedor</th>
                                <th>Cantidad</th>
                                <th>Precio</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>";
            $rowcompras=0;
            $totalcompras=0;
            foreach ($resultadocompras->result() as $item) { 
                $table .= "<tr>
                                <td>".$item->id_detalle_compra."</td>
                                <td>".$item->reg."</td>
                                <td>".$item->producto."</td>
                                <td>".$item->razon_social."</td>
                                <td>".$item->cantidad."</td>
                                <td>".$item->precio_compra."</td>
                                <td>".number_format($item->cantidad*$item->precio_compra,2,'.',',')."</td>
                                
                            </tr>";
                            $rowcompras++;
                            $totalcompras=$totalcompras+($item->cantidad*$item->precio_compra);
                            
            }
            $table.="</tbody> 
                    <tfoot>
                    <td>Compras: </td>
                                <td>".$rowcompras."</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Total:</td>
                                <td><b>$ ".number_format($totalcompras,2,'.',',')."</b></td>
                    </tfoot>
                    </table>";
        }
        $total=0;
        $subtotal=0;
        foreach ($resultadocs->result() as $fila) {
            $total = $fila->total;
            $subtotal = $fila->subtotal;
            $descuento = $fila->descuento;
        }

        $total = round($total,2);
        $subtotal = round($subtotal,2);

        
        $subtotal=number_format($subtotal,2,'.',',');

        //$obedt=number_format($obedt,2,'.',',');




        $array = array("tabla"=>$table,
                        "tabla2"=>'',
                        "dTotal"=>"".$total."",
                        "totalventas"=>$rowventas,
                        "dImpuestos"=>0,
                        "totalutilidad"=>0,// se quitara
                        "descuento"=>$descuento,
                        "dSubtotal"=>"".$subtotal."",
                    );
            echo json_encode($array);
    } 

}