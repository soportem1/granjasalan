	var productoid;
	var productoname;
	var marcaid;
	var marcaname;
	var categoriaid;
	var categorianame;
	var imagenselect;
	var base_url = $('#base_url_g').val();
$(document).ready(function() {
	var idventa;	
	$('#clientes').click(function(event) {
		$('#modalclientes').modal();
		presskey();
	});
	setTimeout(function(){ presskey(); }, 2000);
	$('#realizarventa').click(function(event) {
		var factura = $('#factura').is(':checked')==true?1:0;
                $.ajax({ 
                    type:'POST',
                    url: 'Ventas/ingresarventa',
                    data: {
                    	uss: $('#ssessius').val(),
                        cli: $('#clientes option:selected').val(),
                        total:$('#totalprod').val(),
                        fac:factura,
                        des:$('#optiondescuento option:selected').val(),
                        metodo: $('#metodopagos option:selected').val()
                        
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(data){
                        	console.log(data);
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                    	idventa=data;
                    	var DATA  = [];
            			var TABLA   = $("#tableproductos tbody > tr");
            				TABLA.each(function(){         
				                item = {};
				                item ["idventa"] = idventa;
				                item ["producto"]   = $(this).find("input[id*='vsproid']").val();
				                item ["cantidad"]  = $(this).find("input[id*='vscanti']").val();
				                item ["kilos"]  = $(this).find("input[id*='vskilos']").val();
				                item ["precio"]  = $(this).find("input[id*='vsprecio']").val();
				                DATA.push(item);
				            });
            				INFO  = new FormData();
            				aInfo   = JSON.stringify(DATA);
            				INFO.append('data', aInfo);
				            $.ajax({
				                data: INFO,
				                type: 'POST',
				                url : 'Ventas/ingresarventapro',
				                processData: false, 
				                contentType: false,
				                async: false,
			                    statusCode:{
			                        404: function(data){
			                            toastr.error('Error!', 'No Se encuentra el archivo');
			                        },
			                        500: function(){
			                        	console.log(data.responseText);
			                            toastr.error('Error', '500');
			                        }
			                    },
				                success: function(data){
				                }
				            });
				            var ticket = $('#ticketventa').is(':checked')==true?1:0;
				            var factura = $('#factura').is(':checked')==true?1:0;
				            if (ticket==1) {
				            	$("#modalimpresion").modal();
								$('#iframeticket').html('<iframe src="Visorpdf?filex=Ticket&iden=id&id='+idventa+'"></iframe>');
				            }else{
				            	toastr.success( 'Venta Realizada','Hecho!');
				            	setTimeout(function(){ btncancelar(); }, 2000);
				            }
				            if (factura==1) {
				            	$("#modaldatosfactura").modal();
				            }
				            btncancelar2();
                    }
                });
	});
	$('#selectcliente').mlKeyboard({
          layout: 'es_ES'
        });
	$('#nombredelturno').mlKeyboard({
          layout: 'es_ES'
        });
	$('#cantidadt').mlKeyboard({
          layout: 'es_ES'
        });
	$('#btnabrirt').click(function(){
				$.ajax({
					type:'POST',
					url:'Ventas/abrirturno',
					data:{
						cantidad: $('#cantidadt').val(),
						nombre: $('#nombredelturno').val(),
						bodega: $('#bodegauser').val(),
					},
					async:false,
					statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(data){
                        	console.log(data);
                            toastr.error('Error', '500');
                        }
                    },
					success:function(data){
						toastr.success('Turno Abierto','Hecho!');
						$('#modalturno').modal('hide');
						
					}
				});		
	});

	$('.classproducto').addClass('zoomIn animated');
	$('#secosto').numpad();
    $('#cantidadcajas').numpad();
    $('#cantidadkilos').numpad();
    //$('#cantidadt').numpad();
	$('.panelventa1').click(function(){
		$('.classproducto').removeClass('zoomIn animated');
        $(".panelventa1").css("display","none");
        $(".panelventa2").css("display","block");
        $('.classproductosub').addClass('zoomIn animated');
    });
    $('.panelventa2').click(function(){
    	$('.classproductosub').removeClass('zoomIn animated');
        $(".panelventa2").css("display","none");
        $(".panelventa3").css("display","block");
        $('.classproductotipo').addClass('zoomIn animated');
    });
    $('.panelventa3').click(function(){
    	$('.classproductotipo').removeClass('zoomIn animated');

        $(".panelventa3").css("display","none");
        $(".panelventa4").css("display","block");
        $('input[name=cantidadcajas]').focus(); 
        $('.classsventa').addClass('zoomIn animated');
    });
    $('.masproductos').click(function(event) {
    	$('.panelventa2').html('');
    	$('.panelventa3').html('');
    	$('.classproducto').addClass('zoomIn animated');
    	$(".panelventa4").css("display","none");
    	$(".panelventa1").css("display","block");
    });
    $(".classproducto").click(function(event) {
    	productoid= $(this).attr('data-idproducto');
    	productoname= $(this).attr('data-cate');
    	cargamarcas(productoid);
    	if (productoid!=1) {
    		$("#cantidadkilos").css("display","none");
    		$("#cantidadkilos").val(0);
    	}else{
    		$("#cantidadkilos").css("display","block");
    	}
    });
    //$(".panelventa1").css("display","none");
    //$(".panelventa4").css("display","block");
    $('#savedatosfactura').click(function() {
    	$.ajax({ 
                    type:'POST',
                    url: 'Ventas/datosfactura',
                    data: {
                    	idventa: idventa,
                        datos: $('#datosfactura').val()
                        
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(data){
                        	console.log(data);
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                    	toastr.success( 'Datos Fiscales','Guardados!');
                    }
                });
    });
});

$.fn.numpad.defaults.gridTpl = '<table class="table modal-content tablenumber"></table>';
$.fn.numpad.defaults.backgroundTpl = '<div class="modal-backdrop in"></div>';
$.fn.numpad.defaults.displayTpl = '<input type="text" class="form-control" />';
$.fn.numpad.defaults.buttonNumberTpl =  '<button type="button" class="btn vd_btn vd_bg-yellow numbertouch"></button>';
//$.fn.numpad.defaults.buttonFunctionTpl = '<button type="button" class="btn numbertouch2" style="width: 100%;" onclick="addproductoss()"></button>';
$.fn.numpad.defaults.buttonFunctionTpl = '<button type="button" class="btn numbertouch2" style="width: 100%;"></button>';
$.fn.numpad.defaults.onKeypadCreate = function(){$(this).find('.done').addClass('btn-primary');};

function calculartotal(){
		var addtp = 0;
		$(".vstotal").each(function() {
	        var vstotal = $(this).val();
	        addtp += Number(vstotal);
	    });
	    var descuento0=addtp*$('#optiondescuento option:selected').val();
	    var total=parseFloat(addtp)-parseFloat(descuento0);
	    $('#totalprod').val(total);
	    $('#totalpro').html(Number(total).toFixed(2));
}
function deletepro(id){
		
		$.ajax({
	        type:'POST',
	        url: 'Ventas/deleteproducto',
	        data: {
	            idd: id
	            },
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(data){
	                	console.log(data.responseText);
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	$('.producto_'+id).remove();
	            	calculartotal();
	            }
	        });
	}
function cargamarcas(idp){
	$.ajax({
        type: 'POST',
        url: 'Ventas/cargarmarcas',
        data: {id: idp},
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error!', 'No Se encuentra el archivo');

            },
            500: function(data) {
            	toastr.error('Error', '500');
            	console.log(data.responseText);
            }
        },
        success: function(data) {
        	$('.panelventa2').html('');
            $('.panelventa2').html(data);
        }
    });
}
function marcasselec(idm,mname,imggs){
	marcaid=idm;
	marcaname=mname;
	imagenselect=imggs;
	$.ajax({
        type: 'POST',
        url: 'Ventas/cargarpresentacion',
        data: {pro: productoid,
        		mar:idm},
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(data) {
            	console.log(data.responseText);
            	toastr.error('Error', '500');
            }
        },
        success: function(data) {
        	$('.panelventa3').html('');
            $('.panelventa3').html(data);
        }
    });
}
function presentselec(id,name,precio,Stock){
	categorianame=name;
	categoriaid=id;
	$('.selectimg').html('<img src="'+imagenselect+'" width="100px">');
	$('.selectproducto').html(productoname);
	$('.selectmarca').html(marcaname);
	$('.selectpresentacion').html(categorianame);
	$('.selectcosto').html('$ '+new Intl.NumberFormat('es-MX').format(precio));
	$('.selectstock').html(Stock);
}
function addproductoss(){
	setTimeout(function(){
		$.ajax({
	        type: 'POST',
	        url: 'Ventas/productoverificarstok',
	        data: {
	        		proc: categoriaid,
	        		cant: $('#cantidadcajas').val()
	        	},
	        async: false,
	        statusCode: {
	            404: function(data) {
	                toastr.error('Error!', 'No Se encuentra el archivo');
	            },
	            500: function(data) {
	            	console.log(data.responseText);
	                toastr.error('Error', data);
	            }
	        },
	        success: function(data) {
	        	var array = $.parseJSON(data);
	        	console.log(array);
	        	if (array.estatus==0) {
	        		$('#cantidadcajas').val(array.canti);
	        		toastr.error('No hay suficiente en excistencia, excistencia en stock:'+array.canti ,'Error!');
	        	}else{
	        		setTimeout(function(){ addproductos(); }, 1000);
	        	}





	            
	        }
	    });
    }, 1000);
}
function addproductos(){
	$.ajax({
        type: 'POST',
        url: 'Ventas/addproducto',
        data: {
        		proc: categoriaid,
        		cant: $('#cantidadcajas').val(),
        		kilos: $('#cantidadkilos').val(),
        		producton:productoname,
        		marcan:marcaname,
        		categorian:categorianame
        	},
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(data) {
            	console.log(data.responseText);
                toastr.error('Error', data);
            }
        },
        success: function(data) {
            console.log(data);
		    $('#addproductos').html(data);
		    calculartotal();
		    $('#cantidadkilos').val(0)
        }
    });
}
function pulsadaenter(){
	addproductos();
}
function buscarclientes(){
	var searchcliente=$('#selectcliente').val();
	if (searchcliente.length>3) {
		$.ajax({
	        type:'POST',
	        url: 'Ventas/searchcliente',
	        data: {
	            search: searchcliente
	            },
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(data){
	                	console.log(data.responseText);
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	$('#iframeclientes').html(data);
	            }
	        });
	}
}
function btncancelar(){
		$.ajax({
	        type:'POST',
	        url: 'Ventas/productoclear',
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(){
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	location.reload();
	            }
	        });
}
function btncancelar2(){
		$.ajax({
	        type:'POST',
	        url: 'Ventas/productoclear',
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(){
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	//location.reload();
	            }
	        });
}
function presskey(){
	$('.presskey').click(function() {
		buscarclientes();
	});
}
function addcliente(id,name){
	$('#clientes').html('<option value="'+id+'">'+name+'</option>');
}
function checheddescuento(){
	var descuento = $('#descuento').is(':checked')==true?1:0;
    if (descuento==1) {
        $.ajax({
            type: 'POST',
            url: 'Inicio/descuentov',
            data: {des: descuento},
            async: false,
            statusCode: {
                404: function(data) {
                    notification("topright", "error", "fa fa-exclamation-triangle vd_yellow", "Excepción!", 'No se encuentra el archivo');
                },
                500: function(data) {
                    notification("topright", "error", "fa fa-exclamation-triangle vd_yellow", "Excepción!", data);
                }
            },
            success: function(data) {
              console.log(data);
              if (data==1) {
                

                if ($('#descuento').is(':checked')==true) {
                  $(".divoptiondescuento").css("display", "block");
                }else{
                  $(".divoptiondescuento").css("display", "none");
                  $("#optiondescuento").val(0);
                  calculartotal();
                }

                
                
              }else{
                $(".optiondescuento").css("display", "none");
                $("#optiondescuento").val(0);
                //$('#descuento').prop('checked', false);
              }
            }
        });
    }
}