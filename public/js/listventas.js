var base_url = $('#base_url_g').val();
var table;
var idventa;

$(document).ready(function() {
	table=$('#data-tables-ventas').DataTable();
	loadtable();
	$('#sieditar').click(function(event) {
		var totalventa = $('.totalventa').val();
		var DATAr  = [];
        var TABLA   = $("#itemsventas tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["id"] = $(this).find("input[id*='idvd']").val();
            item ["precio"]   = $(this).find("input[id*='preciovd']").val();
            DATAr.push(item);
        });
        INFO  = new FormData();
        arrayproductos   = JSON.stringify(DATAr);
    	
    	var datos = 'arrayproductos='+arrayproductos+'&total='+totalventa+'&idventa='+idventa;

		
		$.ajax({
            type:'POST',
            url: base_url+'ListaVentas/editarventa',
            data: datos,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
                loadtable();
                toastr.success('Actualización correcta','Hecho!');
                $('#modaledit').modal('hide');
            },
            error: function(response){
            	toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error!');
        	}
        });
	});
	$('#sicancelar').click(function(){
      params = {};
      params.id = $('#hddIdVenta').val();
      $.ajax({
        type:'POST',
        url:base_url+'ListaVentas/cancalarventa',
        data:params,
        async:false,
        success:function(data){
          toastr.success('Cancelado Correctamente','Hecho!');
          //$('#trven_'+params.id).remove();
          loadtable();
        }
      });
    });
});
function loadtable(){
    table.destroy();
    var dia=$('#tipo_dia option:selected').val();
    table = $('#data-tables-ventas').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/ListaVentas/getlistventas",
            type: "post",
            "data": {
                'ro':'0',
                'dia':dia
            },

        },
        "columns": [
            {"data": 'idventa_alias'},
            {"data": 'reg'},
            {"data": 'vendedor'},
            {"data": null,
                    "render" : function ( url, type, full) {
                        
                        var rw=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(full.monto_total);
                        return rw;
                    }
            },
            {"data": 'metodo'},
            {"data": 'cliente'},
            {"data": null,
            		"render" : function ( url, type, full) {
            			var html='';
            			if(full.cancelado==1){
            				html='<span class="badge badge-danger">Cancelado</span>';
            			}
            			return html;
            		}
        	},
            {"data": null,
            	"render" : function ( url, type, full) {
            		var html='';
            				html+='<div class="div_bottms">';
            				html+='<button class="btn btn-raised gradient-blackberry\
            				 white" onclick="ticket('+full.id_venta+')" title="Ticket" data-toggle="tooltip" data-placement="top">\
                                                      <i class="fa fa-book"></i>\
                                                    </button> ';
                               if(full.cancelado==0){ 
                                /*
                                    html+='<button class="btn btn-raised gradient-blackberry white"\
                                    	 	onclick="editventa('+full.id_venta+')" title="Editar" data-toggle="tooltip" data-placement="top">\
                                                            <i class="fa fa-pencil"></i>\
                                                        </button> ';
                                */
                                }
                                html+='<button class="btn btn-raised gradient-flickr white" \
                                		onclick="cancelar('+full.id_venta+','+full.monto_total+')" \
                                		title="Cancelar" data-toggle="tooltip" data-placement="top"';
                                		if(full.cancelado==1){
                                			html+='disabled';
                                		} 
                                		html+='>\
                                                <i class="fa fa-times"></i>\
                                                </button> ';
                            html+='</div>';
            		return html;
            	}
        	},

            
            
            
            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10,25, 50, 100], [10,25, 50,100]],
        "columnDefs": [ {
			"targets": 2,
			"orderable": false
			} ],
		dom: 'Bfrtip',
        buttons: [
        	'pageLength',
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
        
    });
}
function editventa(id){
    idventa=id;
    $('#modaledit').modal();
     $.ajax({
            type:'POST',
            url: base_url+'ListaVentas/itemsventa',
            data: {
                idventa: idventa
            },
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
                $('.itemsventasedit').html(data);
            }
        });
}
function calcularmontos(idventad) {
      var cantidad = $('.cantidad_'+idventad).val();
      var precio = $('.precio_'+idventad).val();
      var totalimport = parseFloat(cantidad)*parseFloat(precio);
      console.log(cantidad+' * '+precio);
      $('.importes_'+idventad).html(totalimport.toFixed(2));

        var addtp = 0;
        $(".importesall").each(function() {
            var vstotal = $(this).html();
            addtp += Number(vstotal);
        });

      var descuento = $('.descuentoventa').data('descuento');


      var descuentot = parseFloat(addtp)*parseFloat(descuento);
        var total = addtp-descuentot;
        total=total.toFixed(2);
      $('.totalventa').val(total);
      $('.totalvental').html(new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(total));
}