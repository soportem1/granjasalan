var base_url = $('#base_url_g').val();
var table;
$(document).ready(function() {
	table=$('#data-tables-compras').DataTable();
	loadtable();
});
function loadtable(){
	var productoscin = $('#productoscin').val();
	var productoscfin = $('#productoscfin').val();
    table.destroy();
    table = $('#data-tables-compras').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Listacompras/getlistcompras",
            type: "post",
            "data": {
                'fechainicio':productoscin,
				'fechafin':productoscfin
            },

        },
        "columns": [
            {"data": 'id_compra'},
            {"data": 'razon_social'},
            {"data": 'monto_total',
            	"render" : function ( url, type, full) {    
                    var rw=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(full.monto_total);
                    return rw;
                }
        	},
            {"data": 'reg'},
            {"data": null,
            	"render" : function ( url, type, full) {
            		var html='';
            		if(full.cancelado==0){
            			html+='Cancelado';
            		} 
                    return html;
            	}
        	},
            {"data": null,
            	"render" : function ( url, type, full) {
            		var html='';
            				html+='<div class="div_bottms">';
            				html+='<button class="btn btn-raised gradient-blackberry\
            				 white" onclick="datoscompras('+full.id_compra+')" title="Ticket" data-toggle="tooltip" data-placement="top">\
                                                      <i class="fa fa-book"></i>\
                                                    </button> ';
                               
                                html+='<button class="btn btn-raised gradient-flickr white" \
                                		onclick="cancelarcp('+full.id_compra+')" \
                                		title="Cancelar" data-toggle="tooltip" data-placement="top"';
                                		if(full.cancelado==0){
                                			html+='disabled';
                                		} 
                                		html+='>\
                                                <i class="fa fa-times"></i>\
                                                </button> ';
                            html+='</div>';
            		return html;
            	}
        	},

            
            
            
            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10,25, 50, 100], [10,25, 50,100]],
        "columnDefs": [ {
			"targets": 2,
			"orderable": false
			} ],
		dom: 'Bfrtip',
        buttons: [
        	'pageLength',
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
        
    });
}
  function datoscompras(id){
    $.ajax({
            type:'POST',
            url: base_url+'Listacompras/comprasdetalle',
            data: {
                compra: id
            },
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
                $('#modalcompraedit').modal();
                $('.modalcompraedit').html(data);
            }
        });
  }
function cancelarcp(id){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Cancelar?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Listacompras/cancelarcompra",
                    data: {
                        compra:id
                    },
                    success:function(response){  
                        toastr.success('Cancelado', 'Hecho!');
                        loadtable();
                        

                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}