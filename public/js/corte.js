$(document).ready(function(){
	$('#chkFecha').change(function(){
		if(document.getElementById("chkFecha").checked){
			f = new Date();
			if(f.getDate()<10){
				dia = "0"+f.getDate();
			}else{dia = f.getDate();
			}
			if(f.getMonth()<10){
				mes = "0"+(f.getMonth()+1);
			}else{
				mes = (f.getMonth()+1);
			}
			fecha = f.getFullYear()+"-"+mes+"-"+dia;
			$('#txtInicio').val(fecha);
			$('#txtFin').val(fecha);
			$('#txtInicio').attr('disabled','disabled');
			$('#txtFin').attr('disabled','disabled');
			
			
		}
		else{
			$('#txtInicio').val('');
			$('#txtFin').val('');
			$('#txtInicio').removeAttr('disabled');
			$('#txtFin').removeAttr('disabled');
		}
	});
	$('#btnBuscar').click(function(){
		params = {};
		params.fecha1 = $('#txtInicio').val();
		params.fecha2 = $('#txtFin').val();
		params.bodega = $('#selectbodega option:selected').val();
		params.informe = $('#selecinforme option:selected').val();
		if(params.fecha1 != '' && params.fecha2 != ''){
			$.ajax({
				type:'POST',
				url:'Corte_caja/corte',
				data:params,
				async:false,
				statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(data){
                        	console.log(data.responseText);
                            toastr.error('Error', '500');
                        }
                    },
				success:function(data){
					var array = $.parseJSON(data);
					$('#tbCorte').html(array.tabla);
					$('#tbCorte2').html(array.tabla2);
					$('#rowventas').html(array.totalventas);
					$('#totalutilidades').html(array.totalutilidad);
					/*$('#cRealizadas').html(array.cRealizadas);
					$('#cContado').html(array.cContado);
					$('#cCredito').html(array.cCredito);
					$('#cTerminal').html(array.cTerminal);
					$('#cCheque').html(array.cCheque);
					$('#cTransferencia').html(array.cTransferencia);
					$('#cPagos').html(array.cPagos);
					$('#dContado').html('$ '+array.dContado);
					$('#dCredito').html('$ '+array.dCredito);
					$('#dTerminal').html('$ '+array.dTerminal);
					$('#dCheque').html('$ '+array.dCheque);
					$('#dTransferencia').html('$ '+array.dTransferencia);
					$('#dPagos').html('$ '+array.dPagos);*/
					$('#dTotal').html(''+array.dTotal);
                    //$('#dImpuestos').html(''+array.dImpuestos);
                    $('#dSubtotal').html(''+array.dSubtotal);
                    $('#sample_2').DataTable({
                    			
						        dom: 'Bfrtip',
						        buttons: [          
						            {
						                extend: 'excel',
						                //messageTop: 'Ventas.',
						                title: 'Corte de ventas'
						            },
						            {
						                extend: 'pdf',
						                //messageTop: 'Ventas.',
						                //messageBottom: null,
						                title: 'Corte de ventas'
						            },
						            'pageLength'
						        ]

						    });
                    $('#sample_3').DataTable({
						        dom: 'Bfrtip',
						        buttons: [          
						            {
						                extend: 'excel',
						                //messageTop: 'Compras.',
						                title: 'Corte de compras'
						            },
						            {
						                extend: 'pdf',
						                //messageTop: 'Compras.',
						                //messageBottom: null,
						                title: 'Corte de compras'
						            },
						        'pageLength'
						        ]
						    });
                    $('#sample_4').DataTable({
						        dom: 'Bfrtip',
						        buttons: [          
						            {
						                extend: 'excel',
						                //messageTop: 'Compras.',
						                title: 'Corte de ventas'
						            },
						            {
						                extend: 'pdf',
						                //messageTop: 'Compras.',
						                //messageBottom: null,
						                title: 'Corte de ventas'
						            },
						        'pageLength'
						        ]
						    });
				}
			});
		}
		else{
			toastr.error('No existen fechas validas', 'Error');
			
		}
	});
});